(include "backwards-compatible-module" )

(backwards-compatible-module (lowdown extended)

(enable-lowdown-extended!
 code-block
 lowdown-extended-html-conversion-rules*)

(import scheme)

(cond-expand
  (chicken-4
    (import chicken)
    (use srfi-1
         (only srfi-14 string->char-set)
         comparse
         lowdown
         lowdown-lolevel
         ))
  (chicken-5
    (import (chicken base)
            (srfi 1)
            (only (srfi 14) string->char-set)
            (comparse)
            (lowdown)
            (lowdown lolevel)
            )))

; --- begin code-block ---

(define ident-charset (string->char-set "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz0123456789-_"))

(define q*
  (as-string (zero-or-more (is #\q))))

(define three-ticks
  (char-seq "```"))

(define non-ticks-line
  (sequence* ((_ space*)
              (l (none-of* three-ticks line))
              )
             (result l)
             ))

(define identifier (as-string (one-or-more (in ident-charset))))

; code-block: parse a three-tick enclosed block of code with an optional language identifier.
; The ticks may optionally be preceded by spaces.
; If no language-identifier is defined, `lang` is false.
(define code-block
  (sequence* ((_ (sequence space* three-ticks))
              (lang (maybe identifier))
              (_ line-end)
              (mytextchunks (zero-or-more non-ticks-line))
              (_ (sequence space* three-ticks line-end))
              )
             (result `(code-block . ,(cons
                                       lang
                                       (cons "\n" mytextchunks)
                                       )))
             ))

; make-code-block: constructs a code-block object with optional language-code
(define (make-code-block lang mytextchunks)
  (if lang
    `(pre (code (@ (class ,(string-append "language-" lang)))
                . ,mytextchunks
                ))
    `(pre (code (@ (foo bar)) . ,mytextchunks))
    ))

; --- end code-block ---

(define lowdown-extended-html-conversion-rules*
  `((code-block . ,(lambda (_ lang_and_body)
                     (make-code-block (car lang_and_body) (cdr lang_and_body))))))

(define (enable-lowdown-extended!)
  (block-hook (cons code-block (block-hook)))
  (markdown-html-conversion-rules*
    (append lowdown-extended-html-conversion-rules*
            (markdown-html-conversion-rules*)))
  (void))

)
