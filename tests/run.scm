(cond-expand
  (chicken-4
   (use posix data-structures))
  (chicken-5
   (import (chicken pretty-print)
           (chicken process)
           (chicken process-context)
           (chicken string))))

(with-output-to-file "lowdown-run"
 (lambda ()
   (print "#!/bin/sh")
   (print "#|")
   (print "exec " (cond-expand
                    (chicken-5 (executable-pathname))
                    (chicken-4 "csi"))
          " -s \"$0\" \"$@\"")
   (print "|#")
   (pretty-print
    `(begin
       ,(cond-expand
          (chicken-4
           `(use lowdown))
          (chicken-5
           `(import (chicken process-context)
                    lowdown)))
       (call-with-input-file (car (command-line-arguments)) markdown->html)))))

(with-output-to-file "lowdown-extended-run"
 (lambda ()
   (print "#!/bin/sh")
   (print "#|")
   (print "exec " (cond-expand
                    (chicken-5 (executable-pathname))
                    (chicken-4 "csi"))
          " -s \"$0\" \"$@\"")
   (print "|#")
   (pretty-print
    `(begin
       ,(cond-expand
          (chicken-4
           `(use lowdown lowdown.extended))
          (chicken-5
           `(import (chicken process-context)
                    lowdown
                    (lowdown extended))))
       (enable-lowdown-extended!)
       (call-with-input-file (car (command-line-arguments)) markdown->html)))))

(system "chmod +x lowdown-run")
(system "chmod +x lowdown-extended-run")

#;(let ((pid
        (process-run "/usr/bin/env"
                     '("perl" "./MarkdownTest_1.0.3/MarkdownTest.pl"
                       "--tidy"
                       "--script" "./lowdown-run"
                       "--testdir" "MarkdownTest_1.0.3/Tests"))))
  (process-wait pid)
  )

(let ((pid
        (process-run "/usr/bin/env"
                     '("perl" "./MarkdownTest_1.0.3/MarkdownTest.pl"
                       "--tidy"
                       "--script" "./lowdown-extended-run"
                       "--testdir" "MarkdownExtensions/Tests"))))
  (process-wait pid)
  )
